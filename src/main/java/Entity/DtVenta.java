/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Kamus
 */
@Entity
@Table(name = "dbo_dtventa")
public class DtVenta implements Serializable {

    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private int id_dtventa;
    @JoinColumn(name="id_producto", referencedColumnName = "id_producto")
    private Producto producto;
    @JoinColumn(name="id_registro", referencedColumnName = "id_registro")
    private Registro registro;
    @Column(name="precio_orden")
    private Double precio_orden;

    public int getId_dtventa() {
        return id_dtventa;
    }

    public void setId_dtventa(int id_dtventa) {
        this.id_dtventa = id_dtventa;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Registro getRegistro() {
        return registro;
    }

    public void setRegistro(Registro registro) {
        this.registro = registro;
    }

    public Double getPrecio_orden() {
        return precio_orden;
    }

    public void setPrecio_orden(Double precio_orden) {
        this.precio_orden = precio_orden;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + this.id_dtventa;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DtVenta other = (DtVenta) obj;
        if (this.id_dtventa != other.id_dtventa) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DtVenta{" + "id_dtventa=" + id_dtventa + '}';
    }
    
    

}
