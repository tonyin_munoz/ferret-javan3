/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Kamus
 */
@Entity
@Table(name = "dbo_registro")
public class Registro implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_registro;
    @Column(name = "nombre_cl")
    private String nombre_cl;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "Tipo_de_pago")
    private String Tipo_de_pago; //enum('credito','contado', 'otro')
    @Column(name = "fecha")
    private String fecha;
    @Column(name = "hora")
    private String hora;
    @Column(name = "total")
    private Double total;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_name")
    private Usuario user;

    public int getId_registro() {
        return id_registro;
    }

    public void setId_registro(int id_registro) {
        this.id_registro = id_registro;
    }

    public String getNombre_cl() {
        return nombre_cl;
    }

    public void setNombre_cl(String nombre_cl) {
        this.nombre_cl = nombre_cl;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipo_de_pago() {
        return Tipo_de_pago;
    }

    public void setTipo_de_pago(String Tipo_de_pago) {
        this.Tipo_de_pago = Tipo_de_pago;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.id_registro;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Registro other = (Registro) obj;
        if (this.id_registro != other.id_registro) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Registro{" + "id_registro=" + id_registro + '}';
    }

    
}
