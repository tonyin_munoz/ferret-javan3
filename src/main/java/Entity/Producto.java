/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Kamus
 */
@Entity
@Table(name = "dbo_producto")
public class Producto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_producto;
    @JoinColumn(name = "id_area", referencedColumnName = "id_area")
    @ManyToOne
    private Area area;
    @Column(name = "nombre_pro")
    private String nombre_pro;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "stock_min")
    private int Stock_min;
    @Column(name = "stock_max")
    private int stock_max;
    @Column(name = "precio_in")
    private double precio_in;
    @Column(name = "precio_out")
    private double precio_out;

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public String getNombre_pro() {
        return nombre_pro;
    }

    public void setNombre_pro(String nombre_pro) {
        this.nombre_pro = nombre_pro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getStock_min() {
        return Stock_min;
    }

    public void setStock_min(int Stock_min) {
        this.Stock_min = Stock_min;
    }

    public int getStock_max() {
        return stock_max;
    }

    public void setStock_max(int stock_max) {
        this.stock_max = stock_max;
    }

    public double getPrecio_in() {
        return precio_in;
    }

    public void setPrecio_in(double precio_in) {
        this.precio_in = precio_in;
    }

    public double getPrecio_out() {
        return precio_out;
    }

    public void setPrecio_out(double precio_out) {
        this.precio_out = precio_out;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id_producto;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        if (this.id_producto != other.id_producto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Producto{" + "id_producto=" + id_producto + '}';
    }

    
}
