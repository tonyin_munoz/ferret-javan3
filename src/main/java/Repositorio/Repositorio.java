package EJB;

import java.util.List;
import javax.ejb.Local;

/*
 * @author Kamus
 */
@Local
public interface Repositorio<T> {

    public void create(T c);

    public void edit(T e);

    public void delete(T d);

    public List<T> findAll();

    public T find(Object id);

}
