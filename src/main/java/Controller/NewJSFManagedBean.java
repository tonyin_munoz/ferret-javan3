/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import EJB.Repositorio;
import Entity.*;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Kamus
 */
@Named
@ViewScoped
public class NewJSFManagedBean implements Serializable{

   @EJB 
   private Repositorio rps;
   private Empleado empleado;
   private List<Empleado> ls;
  
   @PostConstruct
   public void iniciar(){
        
       this.empleado = new Empleado();
       this.ls = rps.findAll();  
   }

    public Repositorio getRps() {
        return rps;
    }

    public void setRps(Repositorio rps) {
        this.rps = rps;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public List<Empleado> getLs() {
        return ls;
    }

    public void setLs(List<Empleado> ls) {
        this.ls = ls;
    }
   
   
}
